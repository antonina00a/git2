﻿using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
        public static int SumOfOddDigits(int n)
        {
            n = Math.Abs(n);
            string nAsText = n.ToString();
            char[] splitted = nAsText.ToCharArray();
            int[] notSorted = new int[splitted.Length];
            for (int i = 0; i < splitted.Length; i++)
            {
                notSorted[i] = int.Parse(splitted[i].ToString());
            }
            int result = 0;
            for (int i = 0; i < notSorted.Length; i++)
            {
                if (notSorted[i]%2!=0)
                {
                    result += notSorted[i];
                }
            }

            return result;
        }

        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            int count = 0;
            while (n > 0)
            {
                count += n & 1;
                n >>= 1;
            }
            return count;
        }

        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            if (n <= 0)
                return 0;

            int[] fibo = new int[n + 1];
            fibo[0] = 0; fibo[1] = 1;

            int sum = fibo[0] + fibo[1];

            for (int i = 2; i <= n; i++)
            {
                fibo[i] = fibo[i - 1] + fibo[i - 2];
                sum += fibo[i];
            }

            return sum;
        }
    }
}