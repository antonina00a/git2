﻿using System;

namespace Condition_statements
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 4; //result = 16
            int b = -5; //result = 5
            int c = 0; //result = 0

            int x = TaskSquared(a);
            Console.WriteLine($"{x}");

            int y = TaskModulus(b);
            Console.WriteLine($"{y}");

            int z = TaskZero(c);
            Console.WriteLine($"{c}");        
        }
        public static int TaskSquared(int a)
        {
            int result = a * a;
            return result;
        }
        public static int TaskModulus(int b)
        {
            int result = Math.Abs(b);
            return result;
        }
        public static int TaskZero(int c)
        {
            int result = c;
            return result;
        }
    }
}
